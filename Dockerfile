FROM ubuntu:latest
MAINTAINER Sébastien Gilles "sebastien.gilles@inria.fr"

RUN (apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q )

RUN apt-get install --no-install-recommends -y laby inkscape \
    && rm -rf /var/lib/apt/lists/*
    
# For some reason, Laby comes with svg files but needs png ones. Use inkscape to convert!
RUN cd /usr/share/laby/tiles \
    &&  inkscape -z -e ant-e.png -w 1024 -h 1024 ant-e.svg \
    && inkscape -z -e ant-n.png -w 1024 -h 1024 ant-n.svg \
    && inkscape -z -e ant-s.png -w 1024 -h 1024 ant-s.svg \
    && inkscape -z -e ant-w.png -w 1024 -h 1024 ant-w.svg \
    && inkscape -z -e exit.png -w 1024 -h 1024 exit.svg \
    && inkscape -z -e nrock.png -w 1024 -h 1024 nrock.svg \
    && inkscape -z -e nweb.png -w 1024 -h 1024 nweb.svg \
    && inkscape -z -e rock.png -w 1024 -h 1024 rock.svg \
    && inkscape -z -e void.png -w 1024 -h 1024 void.svg \
    && inkscape -z -e wall.png -w 1024 -h 1024 wall.svg \
    && inkscape -z -e web.png -w 1024 -h 1024 web.svg
    
RUN apt-get remove -y inkscape \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get -y -q autoclean && apt-get -y -q autoremove 

ENTRYPOINT ["/usr/games/laby", "--lang=fr"]