Command to run Laby from the Docker container (inspired by (taken from https://fredrikaverpil.github.io/2016/07/31/docker-for-mac-and-gui-applications/):

*  Start XQuartz (on macOS only). If not available on your machine you can get it here: https://www.xquartz.org/.

* Make sure _Allow connections from network clients_ is ticked in X11 security preferences.

* Then in a Terminal run 

```
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
```

```
xhost +$ip
```

```
docker run -i -t -e DISPLAY=$ip:0 -v /tmp/.X11-unix:/tmp/.X11-unix --cap-drop=all laby:latest
```